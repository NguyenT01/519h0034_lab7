import 'package:flutter/material.dart';
import 'package:string_util/string_util.dart';

class StringUtilPage extends StatefulWidget {
  const StringUtilPage({Key? key}) : super(key: key);

  @override
  State<StringUtilPage> createState() => _StringUtilPageState();
}

class _StringUtilPageState extends State<StringUtilPage> {
  final formkey = GlobalKey<FormState>();
  final formController = TextEditingController();
  final StringUtil stringUtil = StringUtil();

  String _resultsWC = "";
  String _resultVN = "";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('String util'),
        ),
        body: Center(
          child: Container(
            padding: EdgeInsets.all(40),
            child: Column(
              children: [
                const SizedBox(
                  height: 100,
                ),
                buildForm(),
                const SizedBox(height: 16,),
                const Text('Kết quả sẽ hiển thị tại đây'),
                const SizedBox(height: 16,),
                Text("Word counts: "+_resultsWC, style: TextStyle(fontWeight: FontWeight.bold),),
                const SizedBox(height: 16,),
                Text("Xoá bỏ chữ cái tiếng việt: "+_resultVN, style: TextStyle(fontWeight: FontWeight.bold),),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildForm() {
    return Form(
      key: formkey,
      child: Column(
        children: [
          formField(),
          const SizedBox(
            height: 16,
          ),
          const SizedBox(
            height: 25,
          ),
          submitButton(),
        ],
      ),
    );
  }

  Widget formField() {
    return TextFormField(
      controller: formController,
      decoration: InputDecoration(
          icon: Icon(Icons.account_circle),
          labelText: 'Your name',
          hintText: 'Please enter your name',
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(16),
              borderSide: BorderSide())),
    );
  }

  Widget submitButton() {
    return SizedBox(
      width: double.infinity,
      height: 48,
      child: ElevatedButton(
        child: Text(
          'SUBMIT',
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
        onPressed: validate,
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16))),
      ),
    );
  }

  void validate() {
    final form = formkey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      final data = formController.text;
      setState(() {
        _resultsWC = stringUtil.countWord(data).toString();
        _resultVN = stringUtil.replaceNonVietnamese(data);
      });
    }
  }
}

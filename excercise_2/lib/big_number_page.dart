import 'package:flutter/material.dart';
import 'package:bignumber_util/bignumber_util.dart';

class BigNumberPage extends StatefulWidget {
  const BigNumberPage({Key? key}) : super(key: key);

  @override
  State<BigNumberPage> createState() => _BigNumberPageState();
}

class _BigNumberPageState extends State<BigNumberPage> {
  final formkey = GlobalKey<FormState>();
  final formController = TextEditingController();
  final formController2 = TextEditingController();
  final BigNumberUtil bignum = BigNumberUtil();

  String _results = "";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Bignumber util'),
        ),
        body: Center(
          child: Container(
            padding: EdgeInsets.all(40),
            child: Column(
              children: [
                const SizedBox(
                  height: 50,
                ),
                buildForm(),
                const SizedBox(height: 16,),
                const Text('Kết quả sẽ hiển thị tại đây'),
                const SizedBox(height: 16,),
                Text("Sum: "+_results, style: TextStyle(fontWeight: FontWeight.bold),),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildForm() {
    return Form(
      key: formkey,
      child: Column(
        children: [
          formField1(),
          const SizedBox(
            height: 16,
          ),
          formField2(),
          const SizedBox(
            height: 16,
          ),
          const SizedBox(
            height: 25,
          ),
          submitButton(),
        ],
      ),
    );
  }

  Widget formField1() {
    return TextFormField(
      keyboardType: TextInputType.number,
      controller: formController,
      decoration: InputDecoration(
          labelText: 'Your number1',
          hintText: 'Please enter number1',
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(16),
              borderSide: BorderSide())),
    );
  }
  Widget formField2() {
    return TextFormField(
      keyboardType: TextInputType.number,
      controller: formController2,
      decoration: InputDecoration(
          labelText: 'Your number2',
          hintText: 'Please enter number2',
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(16),
              borderSide: BorderSide())),
    );
  }

  Widget submitButton() {
    return SizedBox(
      width: double.infinity,
      height: 48,
      child: ElevatedButton(
        child: Text(
          'SUBMIT',
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
        onPressed: validate,
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16))),
      ),
    );
  }

  void validate() {
    final form = formkey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      final data = formController.text;
      final data2 = formController2.text;
      setState(() {
        _results = bignum.sum2Numbers(data, data2);
      });
    }
  }
}

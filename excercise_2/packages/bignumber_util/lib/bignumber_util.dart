library bignumber_util;


class BigNumberUtil {
  LinkedList number1 = LinkedList();
  LinkedList number2 = LinkedList();

  BigNumberUtil([String? s1, String? s2]){
    if(s1!= null && s2!=null){
      number1.parseStringToLinkedList(s1);
      number2.parseStringToLinkedList(s2);
    }
  }

  void insertNumbers(String s1, String s2){
    number1.parseStringToLinkedList(s1);
    number2.parseStringToLinkedList(s2);
  }

  String sum2Numbers(String s1, String s2){
    insertNumbers(s1, s2);
    if(number1.head == null){
      if(number2.head ==null){
        return "0";
      }
      else{
        return number2.getString();
      }
    }
    else{
      if(number2.head== null){
        return number1.getString();
      }

      //SUM 2 NUMBERS
      LinkedList tall;
      LinkedList short;

      if(number1.getLength() > number2.getLength()){
        tall = number1;
        short = number2;
      }
      else {
        tall = number2;
        short = number1;
      }

      LinkedList result = LinkedList();

      bool over10 = false;
      while(tall.head !=null){
        if(short.head != null){
          int val1 = (tall.popHead()!.value) ?? 0;
          int val2 = (short.popHead()!.value) ?? 0;
          int sum = val1+val2;
          if(over10){
            sum+=1;
          }
          over10 = (sum>=10);
          sum = sum%10;

          result.addElement(sum);
        }
        else{
          int val1 = (tall.popHead()!.value) ?? 0;
          int sum = val1;
          if(over10){
            sum+=1;
          }
          over10 = (sum>=10);
          sum = sum%10;

          result.addElement(sum);
        }
      }
      if(over10){
        result.addElement(1);
        over10 =false;
      }
      return result.getStringReversed();
    }
  }

}

class Node{
  int? value;
  Node? next;

  Node(this.value, this.next);

}

class LinkedList {
  Node? head;

  void addElement(int number){
    if(head == null){
      head = Node(number, null);
    }
    else{
      Node? previous =  head;
      head = Node(number, previous);
    }
  }

  Node? popHead(){
    if(head == null){
      return null;
    }
    else{
      Node? nextNode = head!.next;
      Node? currentNode = head;
      head = nextNode;

      return currentNode;
    }
  }

  parseStringToLinkedList (String number){
    for(int i=0; i<number.length;i++){
      addElement(int.parse(number[i]));
    }
  }

  String getString(){
    String? data="";
    Node? head2 = head;
    while(head2!=null){
      data = head2.value.toString()+ data!;
      head2 = head2.next;
    }
    return data!;
  }

  String getStringReversed(){
    String? data="";
    Node? head2 = head;
    while(head2!=null){
      data = data! +head2.value.toString();
      head2 = head2.next;
    }
    return data!;
  }


  int getLength(){
    int length=0;
    Node? head2 = head;
    while(head2!=null){
      length++;
      head2 = head2.next;
    }
    return length;
  }

  printLinkedList(){
    String data = "-> ";
    Node? head2 = head;
    while(head2!=null){
      data+= head2.value.toString()+"-> ";
      head2 = head2.next;
    }
    print(data);
  }

}
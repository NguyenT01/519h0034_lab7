import 'package:flutter_test/flutter_test.dart';

import 'package:bignumber_util/bignumber_util.dart';

void main() {
  test('adds one to input values', () {
    final bignumber = BigNumberUtil();
    expect(bignumber.sum2Numbers("2234", "88144"), "90378");
    expect(bignumber.sum2Numbers("2147483647", "9711552884"), "11859036531");
    expect(bignumber.sum2Numbers("5541138221", "9999"), "5541148220");
    expect(bignumber.sum2Numbers("884123668550", "7491136"), "884131159686");
    expect(bignumber.sum2Numbers("4463307712", "9876543210"), "14339850922");
  });
}

library string_util;

/// A Calculator.
class StringUtil {
  int countWord(String name){
    String newName = name.trim().replaceAll(RegExp(' +'), ' ');
    List<String> words = newName.split(' ');
    return words.length;
  }

  String replaceNonVietnamese(String name){
    var withAccent = 'ÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬÉÈẺẼẸÊẾỀỂỄỆÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÍÌỈĨỊÝỲỶỸỴáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựíìỉĩịýỳỷỹỵ';
    var withoutAcc = 'AAAAAAAAAAAAAAAAAEEEEEEEEEEEOOOOOOOOOOOOOOOOOUUUUUUUUUUUIIIIIYYYYYaaaaaaaaaaaaaaaaaeeeeeeeeeeeooooooooooooooooouuuuuuuuuuuiiiiiyyyyy';

    for(int i=0;i<withAccent.length;i++){
      name = name.replaceAll(withAccent[i], withoutAcc[i]);
    }
    return name;
  }

}

import 'package:flutter_test/flutter_test.dart';

import 'package:string_util/string_util.dart';

void main() {
  test('adds one to input values', () {
    final su = StringUtil();
    expect(su.countWord('Le van tam'),3);
    expect(su.countWord('Le van  tam  tu'),4);
    expect(su.replaceNonVietnamese('Nguyễn Văn A'), "Nguyen Van A");
    expect(su.replaceNonVietnamese('Nguyễn ThỊ Mình KhẢi'), "Nguyen ThI Minh KhAi");
  });
}

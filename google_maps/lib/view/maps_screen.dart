import 'package:flutter/material.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MapsScreen extends StatefulWidget {
  const MapsScreen({Key? key}) : super(key: key);

  @override
  State<MapsScreen> createState() => _MapsScreenState();
}

class _MapsScreenState extends State<MapsScreen> {
  Completer<GoogleMapController> _controller = Completer();
  late LatLng userPosition;
  List<Marker> markers = [];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Google Maps'),
          actions: [
            IconButton(onPressed: () => findPlaces(), icon: Icon(Icons.map))
          ],
        ),
        body: Container(
          child: FutureBuilder(
              future: findUserLocation(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return GoogleMap(
                    mapType: MapType.normal,
                    initialCameraPosition:
                        CameraPosition(target: snapshot.data, zoom: 18),
                    markers: Set<Marker>.of(markers),
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                    },
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              }),
        ),
      ),
    );
  }

  Future findPlaces() async {
    final String key = 'AIzaSyCXBxTjERVKItMHKWiIQWgy6LGHiBRCcPI';
    final String placesUrl =
        'https://maps.googleapis.com/maps/api/place/nearbysearch/json?';

    String url = placesUrl +
        'key=$key&type=restaurant&location=${userPosition.latitude},${userPosition.longitude}' + '&radius=1000';

    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      showMarkers(data);
    } else {
      throw Exception('Unable to retrieve places');
    }
  }

  showMarkers(data) {
    List places = data['results'];
    markers.clear();
    places.forEach((place) {
      markers.add(Marker(
          markerId: MarkerId(place['reference']),
          position: LatLng(place['geometry']['location']['lat'],
              place['geometry']['location']['lng']),
          infoWindow:
          InfoWindow(title: place['name'], snippet:
          place['vicinity'])));
    });
    setState(() {
      markers = markers;
    });
  }

  Future<LatLng> findUserLocation() async {
    Location location = Location();
    LocationData userLocation;
    bool active = await location.serviceEnabled();
    PermissionStatus hasPermission = await location.hasPermission();
    if (!active) {
      active = await location.requestService();
    }
    if (hasPermission == PermissionStatus.granted && active) {
      userLocation = await location.getLocation();
      userPosition = LatLng(userLocation.latitude!, userLocation.longitude!);
    } else {
      userPosition = LatLng(10.732784844984284, 106.69982324190667);
    }
    return Future.value(userPosition);
  }
}

import 'package:flutter/material.dart';
import 'maps_screen.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo Google maps',
      home: MapsScreen(),
      debugShowCheckedModeBanner: false,

    );
  }
}
